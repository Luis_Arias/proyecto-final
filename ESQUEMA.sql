DROP DATABASE IF EXISTS heladosArias;
CREATE DATABASE IF NOT EXISTS heladosArias;
USE heladosArias;

CREATE TABLE ventas (
  id int(11) NOT NULL,
  usuario varchar(100) NOT NULL,
  total decimal(10,2) NOT NULL,
  metodoPago varchar(20) NOT NULL,
  fecha datetime NOT NULL DEFAULT current_timestamp(),
  estado varchar(1) DEFAULT '1'
);

CREATE TABLE productos (
  id int(11) NOT NULL,
  nombre varchar(60) NOT NULL,
  descripcion varchar(200) NOT NULL,
  precio decimal(10,2) UNSIGNED NOT NULL,
  urlImagen varchar(500) NOT NULL,
  estado varchar(15) DEFAULT 'activado',
  disponibilidad int(11) UNSIGNED NOT NULL
);

CREATE TABLE usuarios (
  email varchar(100) NOT NULL,
  name varchar(50) NOT NULL,
  password varchar(255) NOT NULL,
  domicilio varchar(60) NOT NULL,
  telefono varchar(15) NOT NULL,
  tipo enum('administrador','usuario') DEFAULT 'usuario'
);

INSERT INTO usuarios (email, name, password, domicilio, telefono, tipo) VALUES
('admin@admin.com', 'admin', '$2b$12$XPT5GtRGMvjno5jT0PtimujTuCuXk.J.dXZHfUCtXHS3ZK.xlDmVm', 'admin', 'admin', 'administrador'),
('user@user.com', 'user', '$2b$12$0RiV2Q2ogKfr2GJwLtMtu.VnSDSAPjy54Gxtb7JPopDrJl0qZUL4e', 'user', 'user', 'usuario');

ALTER TABLE ventas
  ADD PRIMARY KEY (id);

ALTER TABLE productos
  ADD PRIMARY KEY (id);

ALTER TABLE usuarios
  ADD PRIMARY KEY (email);

ALTER TABLE ventas
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE productos
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;