const session = require('express-session');
const bodyParser = require('body-parser');
const express = require('express');
const morgan = require('morgan');
const path = require('path')

// Iniciar puerto 
const app = express();
app.set('port', 4000);
app.listen(app.get('port'), () => {
	console.log('Iniciando en puerto: ', app.get('port'));
});

//Cambiar extensión a archivos ejs
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'ejs');
app.engine('ejs', require('ejs').__express);

//middlewares
app.use(morgan('dev'));

//Ocultar rutas(?
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true,
}))

//Importando rutas
//Relacionado al Login y la sesión
const loginRoutes = require('./routes/login');
const productosRoutes = require('./routes/productos');
const carroRoutes = require('./routes/carro');
//Rutas
app.use('/', productosRoutes);
app.use('/', carroRoutes);
app.use('/', loginRoutes);

app.get('/', (req, res) => {
	if (req.session.loggedin == true) {
		res.render('usuarios/shopping', { name: req.session.name });
	} else {
		res.redirect('/login');
	}
});

app.get('/admin/home', (req, res) => {
	if (req.session.loggedin == true) {
		res.render('admin/home', { name: req.session.name });
	} else {
		res.redirect('/login');
	}
});

//Comprobación de Admin o Usuario
app.get('/nosotros', (req, res) => {
	if (req.session.loggedin == true) {
		res.render('usuarios/nosotros', { name: req.session.name });
	} else {
		res.redirect('/login');
	}
});

app.get('/faqs', (req, res) => {
	if (req.session.loggedin == true) {
		res.render('usuarios/faqs', { name: req.session.name });
	} else {
		res.redirect('/login');
	}
});

//Archivos estáticos
app.use(express.static(path.join(__dirname, 'public')));

//Imágenes 
app.use('/uploads', express.static('uploads'));

