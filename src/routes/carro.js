const express = require('express');
const pool = require('../bd');
const router = express.Router();

const carroController = require('../controllers/carroController');

router.get('/shopping', carroController.listShopping);
router.get('/getProductos', (req, res) => {
	pool.query('SELECT * FROM productos', (err, productos) => {
		if (err) {
			res.json(err);
		}
		res.json(productos);
	})
})

router.post('/comprar', (req, res) => {
	let { carrito, metodo } = req.body;

	try {
		carrito = JSON.parse(carrito)
	} catch (error) {
		return res.status(400).send("Carrito inválido")
	}

	let total = 0;
	for (const [key, value] of Object.entries(carrito)) {
		total += value.precio * value.cantidad;

	}
	pool.query('INSERT INTO ventas (total, metodoPago, usuario) VALUES (?, ?, ?)', [total, metodo, req.session.email])
})

module.exports = router;