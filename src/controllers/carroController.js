const pool = require('../bd');
const controller = {};

controller.list = (req, res) => {
    pool.query('SELECT * FROM productos', (err, productos) => {
        if (err) {
            res.json(err);
        }
        if (req.session.loggedin == true) {
            res.render('usuarios/carro', { name: req.session.name, data: productos, });
        } else {
            res.redirect('/login');
        }
    });
};

controller.listShopping = (req, res) => {
    pool.query('SELECT * FROM productos', (err, productos) => {
        if (err) {
            res.json(err); //next(err);
        }
        if (req.session.loggedin == true) {
            res.render('usuarios/shopping', { name: req.session.name, data: productos, });
        } else {
            res.redirect('/login');
        }
    });
};

module.exports = controller;