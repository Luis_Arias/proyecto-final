const mysql = require('mysql2');
const { config } = require('dotenv');

config();

//Conexión Base de Datos
const pool = mysql.createConnection({
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	password: process.env.DB_PASS,
	port: process.env.DB_PORT,
	database: process.env.DB_NAME
});

module.exports = pool;
